/* CAROLINA ECHIBURU // 20.171.816-3 */


//CODIGO DE CONTROL 5 Y CONTROL 6 CON SUS RESPECTIVAS FUNCIONES.


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "estudiantes.h"

float desvStd(estudiante curso[])
{
	int i;
	float mediaAritmetica = 0.0, varianza = 0.0, desviacionEstandar;
	for (i = 1; i < 30; i++)
	{
		mediaAritmetica = curso[i].prom + mediaAritmetica;
	}
	mediaAritmetica = (mediaAritmetica)/30;
	for (i = 1; i < 30; i++)
	{
		varianza = pow(curso[i].prom - mediaAritmetica, 2) + varianza;
	}
	desviacionEstandar = sqrt(varianza/29);
	printf("La desviacion estandar del curso es: %.2f\n", desviacionEstandar);
	return 0.0;
}

float menor(float prom[])
{
	int i;
	float menorProm = prom[0];
	for (i = 1; i < 30; i++)
	{
		if (menorProm > prom[i])
		{
			menorProm = prom[i];
		}
	}
	printf("El promedio mas bajo fue: %.2f\n", menorProm);
	return 0.0;
}

float mayor(float prom[])
{
	int i;
	float mayorProm = prom[0];
	for (i = 1; i < 30; i++)
	{
		if (mayorProm < prom[i])
		{
			mayorProm = prom[i];
		}
	}
	printf("El promedio mas alto fue: %.2f\n", mayorProm);
	return 0.0;
}

void registroCurso(estudiante curso[])
{
	int i;
	float promProy, promCont;
	for (i = 0; i < 24; i++)
	{
		printf("\nALUMNO: %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		printf("Ingrese las notas de los proyectos(3): \n");
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3);
		printf("Ahora ingrese las notas de los controles(6): \n");
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, &curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, &curso[i].asig_1.cont6);
		promProy = (curso[i].asig_1.proy1)*0.2 + (curso[i].asig_1.proy2)*0.2 + (curso[i].asig_1.proy3)*0.3;
		promCont = (curso[i].asig_1.cont1)*0.05 + (curso[i].asig_1.cont2)*0.05 + (curso[i].asig_1.cont3)*0.05 + (curso[i].asig_1.cont4)*0.05 + (curso[i].asig_1.cont5)*0.05 + (curso[i].asig_1.cont6)*0.05;
		curso[i].prom = (promProy + promCont);
	}
	//printf("TEST"); 
}

void clasificarEstudiantes(char path[], estudiante curso[])
{
	int i;
	FILE *reprobados;
	FILE *aprobados;
	reprobados = fopen("estudiantesReprobados.txt", "w");
	aprobados = fopen("estudiantesAprobados.txt", "w");
	for (i = 0; i < 30; i++)
	{
		if (curso[i].prom < 40)
		{
			fprintf(reprobados, "%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
		else
		{
			fprintf(aprobados, "%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
	}
	fclose(reprobados);
	fclose(aprobados);
	//printf("TEST"); 
}


void metricasEstudiantes(estudiante curso[])
{
	float desvStd(estudiante curso[]);
	float menor(float prom[]);
	float mayor(float prom[]);
	//printf("TEST"); 
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes");
		printf( "\n   2. Ingresar notas");
		printf( "\n   3. Mostrar Promedios");
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes");
		printf( "\n   6. Salir.");
		printf( "\n\n   Introduzca opción (1-6): ");
		scanf( "%d", &opcion);
        switch (opcion){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;	
					
      case 5: clasificarEstudiantes("destino", curso); // clasi
        	break;
         }

    } while (opcion != 6);
}

int main()
{
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}